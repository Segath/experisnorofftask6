﻿using System;

namespace Task6_BMICalc
{
    class Program
    {
        static void Main(string[] args)
        {
            double weight = UserInput("Type your weight in kg: ");
            double height = UserInput("Type your height in meters: ");

            double bmi = weight / Math.Pow(height, 2);

            if(bmi < 18.5)
            {
                Console.WriteLine("You are Underweight");
            }
            else if(bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine("You are Normal Weight");
            }
            else if (bmi >= 25 && bmi < 30)
            {
                Console.WriteLine("You are Overweight");
            }
            else if (bmi > 30)
            {
                Console.WriteLine("You are Obese");
            }
        }

        // Reads and checks if the user input is valid
        public static double UserInput(string msg)
        {
            double value = -1;
            do
            {
                Console.Write(msg);
                try
                {
                    value = double.Parse(Console.ReadLine().Replace(".", ","));
                }
                catch (FormatException)
                {
                    Console.WriteLine("Type a number.");
                }
            } while (value == -1);

            return value;
        }
    }
}
